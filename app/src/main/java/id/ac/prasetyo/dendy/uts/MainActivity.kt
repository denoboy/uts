package id.ac.prasetyo.dendy.uts

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragSpd : FragmentSepeda
    lateinit var fragMember : FragmentTrans
    lateinit var ft : FragmentTransaction


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSepeda.setOnClickListener(this)
        btnTransaksi.setOnClickListener(this)
        btnHome.setOnClickListener(this)

        fragSpd = FragmentSepeda()
        fragMember = FragmentTrans()
        db = DBOpenHelper(this).writableDatabase

    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSepeda->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragSpd).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,255,225))
                framelayout.visibility = View.VISIBLE
            }
            R.id.btnTransaksi->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragMember).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,255,225))
                framelayout.visibility = View.VISIBLE
            }
            R.id.btnHome -> framelayout.visibility = View.GONE
            }
        }
    }