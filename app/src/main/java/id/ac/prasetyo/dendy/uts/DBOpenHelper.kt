package id.ac.prasetyo.dendy.uts

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper (context : Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {

    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs        = "create table member(id_member integer primary key autoincrement, nama text not null, id_spd int not null)"
        val tProdi      = "create table sepeda(id_spd integer primary key autoincrement, nama_spd text not null)"
        val insProdi    = "insert into sepeda(nama_spd) values('BMX'),('BECAK'),('ONTHEL')"

        db?.execSQL(tMhs)
        db?.execSQL(tProdi)
        db?.execSQL(insProdi)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object {
        val DB_Name = "sepeda"
        val DB_Ver = 1
    }


}