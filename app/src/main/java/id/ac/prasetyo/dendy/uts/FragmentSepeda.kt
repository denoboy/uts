package id.ac.prasetyo.dendy.uts

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.sepeda.view.*
import java.lang.StringBuilder

class FragmentSepeda : Fragment (), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertSP ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }

            R.id.btnUpdateSP ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }

            R.id.btnHapusSP ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var v : View
    lateinit var builder: AlertDialog.Builder
    var idSpd : String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v = inflater.inflate(R.layout.sepeda,container,false)
        v.btnUpdateSP.setOnClickListener(this)
        v.btnHapusSP.setOnClickListener(this)
        v.btnInsertSP.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsSepeda.setOnItemClickListener(itemClick)
        return v
    }

    fun showDataSepeda (){
        val cursor : Cursor = db.query("sepeda", arrayOf("nama_spd","id_spd as _id"),null,null,null,null,"id_spd asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_sepeda,cursor, arrayOf("_id", "nama_spd"),
            intArrayOf(R.id.txIdSepeda,R.id.txNamaSepeda),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsSepeda.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataSepeda()
    }
    
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idSpd = c.getString(c.getColumnIndex("_id"))
        v.edNamaSepeda.setText(c.getString(c.getColumnIndex("nama_spd")))
    }

    fun insertDataSepeda(namaSepeda: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_spd",namaSepeda)
        db.insert("sepeda",null,cv)
        showDataSepeda()
    }

    fun updateDataSepeda(namaSepeda: String, idSpd: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_spd",namaSepeda)
        db.update("sepeda",cv,"id_spd = $idSpd",null)
        showDataSepeda()
    }

    fun deleteDataSepeda(idSpd: String){
        db.delete("sepeda","id_spd = $idSpd",null)
        showDataSepeda()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataSepeda(v.edNamaSepeda.text.toString())
        v.edNamaSepeda.setText("")
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataSepeda(v.edNamaSepeda.text.toString(),idSpd)
        v.edNamaSepeda.setText("")
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataSepeda(idSpd)
        v.edNamaSepeda.setText("")
    }
}