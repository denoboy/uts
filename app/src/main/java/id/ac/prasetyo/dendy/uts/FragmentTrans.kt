package id.ac.prasetyo.dendy.uts

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.transaksi.*
import kotlinx.android.synthetic.main.transaksi.view.*

class FragmentTrans :  Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namasepeda = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){

            R.id.btnInsertMember ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }

        }
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namasepeda : String = ""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.transaksi,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)


        v.btnInsertMember.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataMhs()
        showDataProdi()
    }

    fun showDataMhs(){

        var sql = "select m.id_member as _id, m.nama, p.nama_spd from member m, sepeda p " +
                "where m.id_spd = p.id_spd order by m.nama asc"
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_member,c,
            arrayOf("_id","nama","nama_spd"), intArrayOf(R.id.txNmr, R.id.txNamaMm, R.id.txNamaSpd),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMhs.adapter = lsAdapter
    }

    fun showDataProdi(){
        val c : Cursor = db.rawQuery("select nama_spd as _id from sepeda order by nama_spd asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    fun insertDataMhs(namamember: String,id_spd : Int){
        var sql = "insert into member(nama, id_spd) values (?,?)"
        db.execSQL(sql, arrayOf(namamember,id_spd))
        showDataMhs()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_spd from sepeda where nama_spd='$namasepeda'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            insertDataMhs(v.edNamaMember.text.toString(),
                c.getInt(c.getColumnIndex("id_spd")))
            v.edNamaMember.setText("")
        }
    }


}